<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="app.css">
    <script src="app.js"></script>
    <title>Document</title>
</head>

<body>
    <article>

        <h1><a href="posts/my-first-post"> My First Post</a></h1>

        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed doloremque voluptatem maxime tempore ipsa magnam
            exercitationem? Exercitationem, minus reiciendis ipsa explicabo repellendus sed recusandae voluptas cum
            culpa.
            Ullam beatae, placeat incidunt voluptatem mollitia cupiditate nisi atque laborum. Nobis nisi consequatur
            harum
            alias enim dolores saepe libero ipsum neque, omnis ratione odio molestias vitae perferendis minus, aut
            debitis
            numquam odit tenetur ipsa provident pariatur eos. Laborum odit suscipit ut perspiciatis soluta? Cumque
            minus,
            architecto totam blanditiis velit, odit veniam, facere repellat deserunt consectetur vel voluptates!
            Obcaecati,
            facere quisquam, aliquam incidunt adipisci quam eum iure, labore porro eligendi dolores dignissimos est
            numquam!
        </p>
    </article>
    <article>

        <h1><a href="posts/my-second-post"> My Second Post</a></h1>

        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed doloremque voluptatem maxime tempore ipsa magnam
            exercitationem? Exercitationem, minus reiciendis ipsa explicabo repellendus sed recusandae voluptas cum
            culpa. Ullam
            beatae, placeat incidunt voluptatem mollitia cupiditate nisi atque laborum. Nobis nisi consequatur harum
            alias enim
            dolores saepe libero ipsum neque, omnis ratione odio molestias vitae perferendis minus, aut debitis numquam
            odit
            tenetur ipsa provident pariatur eos. Laborum odit suscipit ut perspiciatis soluta? Cumque minus, architecto
            totam
            blanditiis velit, odit veniam, facere repellat deserunt consectetur vel voluptates! Obcaecati, facere
            quisquam,
            aliquam incidunt adipisci quam eum iure, labore porro eligendi dolores dignissimos est numquam!</p>
    </article>

    <article>

        <h1><a href="posts/my-third-post"> My Third Post</a></h1>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sed doloremque voluptatem maxime tempore ipsa magnam
            exercitationem? Exercitationem, minus reiciendis ipsa explicabo repellendus sed recusandae voluptas cum
            culpa. Ullam
            beatae, placeat incidunt voluptatem mollitia cupiditate nisi atque laborum. Nobis nisi consequatur harum
            alias enim
            dolores saepe libero ipsum neque, omnis ratione odio molestias vitae perferendis minus, aut debitis numquam
            odit
            tenetur ipsa provident pariatur eos. Laborum odit suscipit ut perspiciatis soluta? Cumque minus, architecto
            totam
            blanditiis velit, odit veniam, facere repellat deserunt consectetur vel voluptates! Obcaecati, facere
            quisquam,
            aliquam incidunt adipisci quam eum iure, labore porro eligendi dolores dignissimos est numquam!</p>
    </article>

</body>

</html>
