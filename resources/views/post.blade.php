<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="app.css">
    <script src="app.js"></script>
    <style>
        body {
            max-width: 600px;
            margin: auto;
            line-height: 1.6;
        }
        p{
            font-size: 20px;
        }

    </style>
    <title>Document</title>
</head>

<body>
    <article>
        {!! $post !!}

        <a href="/">Go Back</a>

</body>

</html>
